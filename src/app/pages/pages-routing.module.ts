import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../auth/auth.guard';
import { ApprovalComponent } from './customer-panel/approval/approval.component';
import { CutoffComponent } from './customer-panel/cutoff/cutoff.component';
import { OverviewComponent } from './customer-panel/overview/overview.component';
import { PanelSpesialComponent } from './customer-panel/panel-spesial/panel-spesial.component';
import { PanelComponent } from './customer-panel/panel/panel.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PagesComponent } from './pages.component';
import { AddNewClinicComponent } from './customer-clinic/add-new-clinic/add-new-clinic.component';
import { ClinicEditComponent }from './customer-clinic/clinic-edit/clinic-edit.component';

const routes: Routes = [  
  {
    path: '',
    component: PagesComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'dashboard',
        component: DashboardComponent,
        canActivateChild: [AuthGuard]
      },
      {
        path: 'panel',
        component: PanelComponent,
        canActivateChild: [AuthGuard]
      },
      {
        path: 'panel-spesial',
        component: PanelSpesialComponent,
        canActivateChild: [AuthGuard]
      },
      {
        path: 'approval-panel',
        component: ApprovalComponent,
        canActivateChild: [AuthGuard]
      },
      {
        path: 'cut-off-panel',
        component: CutoffComponent,
        canActivateChild: [AuthGuard]
      },
      {
        path: 'overview-panel',
        component: OverviewComponent,
        canActivateChild: [AuthGuard]
      },
      {
        path: 'clinic-edit',
        component: ClinicEditComponent,
        canActivateChild: [AuthGuard]
      },
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
      },
      {
        path : 'add-new-clinic',
        component: AddNewClinicComponent,
        canActivateChild: [AuthGuard]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
