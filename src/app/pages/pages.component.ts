import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { MenuItem } from 'primeng/api';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.css']
})
export class PagesComponent implements OnInit {

  public profileUser: any;
  public items: MenuItem[];
  private currentdate: any = "Default"
  public breadcrumbList: MenuItem[];
  public sidebarMenuList: MenuItem[];

  constructor(private datePipe: DatePipe, private router: Router, private authService: AuthService) {
    this.profileUser = this.authService.getStorage('EmployeeName');
    let currdate=new Date();
    this.currentdate = this.datePipe.transform(currdate, 'EEEE, dd-MMM-yy')
    this.items = [
      {
        // label: this.username,
        icon: 'pi pi-user',
        items: [
          {
            label: 'My Account',
            icon: 'pi pi-user-edit'
            // command: () => {
            //   this.toProfile();
            // },
          },
          {
            label: '',
            separator: true,
          },
          {
            label: 'Sign out',
            icon: 'pi pi-fw pi-sign-out'
            // command: () => {
            //   this.showDialog();
            // }
          },
        ],
      },
      {
        label: this.currentdate,
      },
    ];
    this.breadcrumbList = [
      {label: 'Home', icon: 'pi pi-home', routerLink: '/mgm/home'},
      {label: 'Dashboard'}
    ];
    this.sidebarMenuList = [
      {label: 'Home', icon: 'pi pi-fw pi-home', routerLink: '/dashboard'},
      {
        label: 'Master Data', icon: 'pi pi-fw pi-align-justify',
        items: [
          {
            label: 'Customer Outlet',
            items: [
              {
                label: 'Customer Panel',
                items: [
                  {
                    label: 'Customer Outlet Panel',
                    routerLink: '/panel'
                  },
                  {
                    label: 'Customer Outlet Panel Spesial',
                    routerLink: '/panel-spesial'
                  },
                  {
                    label: 'Approval Customer Outlet',
                    routerLink: '/approval-panel'
                  },
                  {
                    label: 'Cut Off Customer Outlet Panel & Spesial',
                    routerLink: '/cut-off-panel'
                  },
                  {
                    label: 'Overview Customer Outlet Panel',
                    routerLink: '/overview-panel'
                  }
                ]
              },{
                label: 'Customer Clinic',
                items: [
                  {
                    label: 'Clinic Master',
                    items: [
                      {
                        label: 'Add New CLinic',
                        routerLink: '/add-new-clinic'
                      },
                      {
                        label: 'Open/Edit New Clinic',
                        routerLink: '/clinic-edit'
                      },
                      {
                        label: 'Approval New Clinic',
                        routerLink: '/panel-spesial'
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ]
      }
    ];
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        const url = event.urlAfterRedirects;
        if (url === '/dashboard') {
          this.breadcrumbList = [
            {label: 'Home', icon: 'pi pi-home'}
          ];
        } else {
          // const routerLabel: any = router.getCurrentNavigation();
          const routerLabel = this.getNestedRouterLink(this.sidebarMenuList, url);
          this.breadcrumbList = [
            {label: 'Home', icon: 'pi pi-home', routerLink: '/dashboard'},
            {label: routerLabel}
          ];
        }
      }
    });
  }

  getNestedRouterLink(nextItem: any[], currentUrl: any): any {
    for (let index = 0; index < nextItem.length; index++) {
      const item = nextItem[index];
      if (item.routerLink === currentUrl) {
        return item.label;
      } else {
        if (item.items !== undefined && item.items.length > 0) {
          return this.getNestedRouterLink(item.items, currentUrl);
        }
      }
    }
  }

  ngOnInit(): void {
  }

}
