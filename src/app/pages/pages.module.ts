import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ToolbarModule } from 'primeng/toolbar';
import { ButtonModule } from 'primeng/button';
import { BadgeModule } from "primeng/badge";
import { ImageModule } from 'primeng/image';
import { DividerModule } from 'primeng/divider';
import { MenubarModule } from 'primeng/menubar';
import { MenuModule } from 'primeng/menu';
import { SplitButtonModule } from 'primeng/splitbutton';
import { PanelModule } from 'primeng/panel';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { PanelMenuModule } from 'primeng/panelmenu';
import { DropdownModule } from 'primeng/dropdown';
import { CalendarModule } from 'primeng/calendar';
import { DialogService, DynamicDialogModule } from 'primeng/dynamicdialog';
import { TableModule } from 'primeng/table';
import { InputTextModule } from 'primeng/inputtext';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { ToastModule } from 'primeng/toast';

import { PagesRoutingModule } from './pages-routing.module';
import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PanelComponent } from './customer-panel/panel/panel.component';
import { PanelSpesialComponent } from './customer-panel/panel-spesial/panel-spesial.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CustomerComponent } from './popup/customer/customer.component';
import { TableComponent } from './generic/table/table.component';
import { ConfirmationService, MessageService } from 'primeng/api';
import { ApprovalComponent } from './customer-panel/approval/approval.component';
import { CutoffComponent } from './customer-panel/cutoff/cutoff.component';
import { OverviewComponent } from './customer-panel/overview/overview.component';
import { AddNewClinicComponent } from './customer-clinic/add-new-clinic/add-new-clinic.component';

import { ClinicEditComponent }from './customer-clinic/clinic-edit/clinic-edit.component';
import { CustomerClinicModalComponent }from './popup/customerclinic/customerclinicmodal.component';
import { BlockUIModule } from 'primeng/blockui';
import { ProgressSpinnerModule } from 'primeng/progressspinner';


@NgModule({
  declarations: [
    PagesComponent,
    DashboardComponent,
    PanelComponent,
    PanelSpesialComponent,
    CustomerComponent,
    TableComponent,
    ApprovalComponent,
    CutoffComponent,
    OverviewComponent,
    AddNewClinicComponent,
    ClinicEditComponent,
    CustomerClinicModalComponent
 
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    PagesRoutingModule,
    ToolbarModule,
    ButtonModule,
    BadgeModule,
    ImageModule,
    DividerModule,
    MenubarModule,
    MenuModule,
    SplitButtonModule,
    PanelModule,
    BreadcrumbModule,
    PanelMenuModule,
    DropdownModule,
    CalendarModule,
    DynamicDialogModule,
    TableModule,
    InputTextModule,
    ConfirmDialogModule,
    MessagesModule,
    MessageModule,
    BlockUIModule,
    ProgressSpinnerModule,
    ToastModule
  ],
  providers: [
    DatePipe,
    DialogService,
    MessageService,
    ConfirmationService
  ]
})
export class PagesModule { }
