import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ConfirmationService, Message, MessageService } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { AuthService } from 'src/app/auth/auth.service';
import { CustomerComponent } from '../../popup/customer/customer.component';

@Component({
  selector: 'app-panel-spesial',
  templateUrl: './panel-spesial.component.html',
  styleUrls: ['./panel-spesial.component.css']
})
export class PanelSpesialComponent implements OnInit {

  formMessage: Message[] = [];
  loading: boolean[] = [false, false, false];
  areaList: any[];
  liniList: any[];
  customerAreaList: any[];
  filterAreaList: any[];
  channelList: any[];
  panelSpecialForm: FormGroup;
  customerDialog: DynamicDialogRef = new DynamicDialogRef;
  selectedCustomerList: any[] = [];

  constructor(private formBuilder: FormBuilder, public dialogService: DialogService, 
    public datePipe: DatePipe, private messageService: MessageService,
    private confirmationService: ConfirmationService, private authService: AuthService) {
    const today: Date = new Date();
    const tempStartDate: Date = new Date(Date.UTC(today.getFullYear(), today.getMonth(), 1));
    const tempEndDate: Date = new Date(Date.UTC((today.getFullYear() + 10), 11, 31));
    this.filterAreaList = [];
    this.areaList = [];
    this.customerAreaList = [];
    this.liniList = [];
    this.channelList = [];
    this.panelSpecialForm = this.formBuilder.group({
      area: ['', Validators.required],
      lini: ['', Validators.required],
      customerArea: ['', Validators.required],
      channel: ['', Validators.required],
      StartDate: [tempStartDate, Validators.required],
      EndDate: [tempEndDate, Validators.required],
      customerList: this.formBuilder.array([])
    });
  }

  ngOnInit(): void {
    this.onLoadArea();
    this.onLoadLine();
    this.onLoadChannel();
  }

  onLoadArea() {
    this.loading[0] = true;
    this.authService.postMethod('/api/dexaapi/area/list', {})
    .subscribe({
      next: data => {
        this.loading[0] = false;
        if (data) {
          this.areaList = data;
          this.filterAreaList = this.areaList;
          this.customerAreaList = this.areaList;
        } else {
          this.areaList = [{
            AreaID:'',
            AreaCode:"",
            AreaName:"No Data Found"
          }];
          this.filterAreaList = this.areaList;
          this.customerAreaList = this.areaList;
        }
      },
      error: error => {
        this.loading[0] = false;
        this.areaList = [{
          AreaID:'',
          AreaCode:"",
          AreaName:"No Data Found"
        }];
        this.filterAreaList = this.areaList;
        this.customerAreaList = this.areaList;
        if (error.error.code === 'INVALID TOKEN') {
          this.authService.clear();
        } else {
          this.messageService.add({key: 'globalToast', severity:'error', summary: 'Error', detail: error.error.message});
        }
      }
    });
  }

  onLoadLine() {
    this.loading[1] = true;
    this.authService.postMethod('/api/dexaapi/line/list', {})
    .subscribe({
      next: data => {
        this.loading[1] = false;
        if (data) {
          this.liniList = data;
        } else {
          this.liniList = [{
            LineID:'',
            LineName:"No Data Found"
          }];
        }
      },
      error: error => {
        this.loading[1] = false;
        this.liniList = [{
          LineID:'',
          LineName:"No Data Found"
        }];
        if (error.error.code === 'INVALID TOKEN') {
          this.authService.clear();
        } else {
          this.messageService.add({key: 'globalToast', severity:'error', summary: 'Error', detail: error.error.message});
        }
      }
    });
  }

  onLoadChannel() {
    this.loading[2] = true;
    this.authService.postMethod('/api/dexaapi/channel/list', {})
    .subscribe({
      next: data => {
        this.loading[2] = false;
        if (data) {
          this.channelList = data;
        } else {
          this.channelList = [{
            ChannelID:'',
            ChannelCode:"",
            ChannelName:"No Data Found",
            GroupName:""
          }];
        }
      },
      error: error => {
        this.loading[2] = false;
        this.channelList = [{
          ChannelID:'',
          ChannelCode:"",
          ChannelName:"No Data Found",
          GroupName:""
        }];
        if (error.error.code === 'INVALID TOKEN') {
          this.authService.clear();
        } else {
          this.messageService.add({key: 'globalToast', severity:'error', summary: 'Error', detail: error.error.message});
        }
      }
    });
  }

  get customerList() {
    return this.panelSpecialForm.get('customerList') as FormArray;
  }

  onChangeAreaLine() {
    let tempArea = this.panelSpecialForm.value.area.AreaID;
    if (tempArea) {
      this.customerAreaList = this.filterAreaList.filter((customerArea: any) => {
        return customerArea.AreaID !== tempArea;
      });
    } else {
      this.customerAreaList = this.filterAreaList;
    }
  }

  onAddCustomer() {
    let tempArea = this.panelSpecialForm.value.customerArea;
    if (tempArea) {
      this.customerDialog = this.dialogService.open(CustomerComponent, {
        header: 'Customer List',
        width: '70%',
        contentStyle: {"max-height": "500px", "overflow": "auto"},
        baseZIndex: 10000,
        data: {
          CompanyID: this.authService.getStorage("CompanyID"),
          CompanyName: this.authService.getStorage("CompanyName"),
          areaId: tempArea.AreaID,
          areaName: tempArea.AreaName
        }
      });
  
      this.customerDialog.onClose.subscribe((selectedDialogList: any[]) => {
        if (selectedDialogList !== undefined) {
          if (selectedDialogList.length > 0 && this.customerList.value.length > 0) {
            let resultDiffer = selectedDialogList.filter((item: any) => 
            !this.customerList.value.some((existing: any) => existing.Sitecode === item.Sitecode));
            resultDiffer.filter((item : any) => this.customerList.value.push(item));
          } else {
            selectedDialogList.filter((item : any) => this.customerList.value.push(item));
          }
          this.selectedCustomerList = [];
        }
      });
    }
  }

  ngOnDestroy() {
    if (this.customerDialog) {
        this.customerDialog.close();
    }
  }

  onDeleteCustomer() {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete the selected Customer ?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        let result = this.customerList.value.filter((item: any) => 
        !this.selectedCustomerList.includes(item));
        this.customerList.setValue([]);
        result.filter((item : any) => this.customerList.value.push(item));
        this.messageService.add({severity:'success', summary:'Successful', detail:'Data has been deleted', life: 500});
        this.selectedCustomerList = [];
      }
    });
  }

  onSubmit() {
    this.panelSpecialForm.value.StartDate = new Date(Date.UTC(
      this.panelSpecialForm.value.StartDate.getFullYear(),
      this.panelSpecialForm.value.StartDate.getMonth(),
      this.panelSpecialForm.value.StartDate.getDate()));
    this.panelSpecialForm.value.EndDate = new Date(Date.UTC(
      this.panelSpecialForm.value.EndDate.getFullYear(),
      this.panelSpecialForm.value.EndDate.getMonth(),
      this.panelSpecialForm.value.EndDate.getDate()));
      if (this.panelSpecialForm.value.StartDate.getDate() !== 1) {
        this.messageService.add({ severity: 'error', summary: 'Failed', detail: 'start date have to selected with early day of the month', life: 1 });
        return;
      }
      const tempPeriodTo = new Date(Date.UTC(
        this.panelSpecialForm.value.EndDate.getFullYear(),
        this.panelSpecialForm.value.EndDate.getMonth(),
        this.panelSpecialForm.value.EndDate.getDate() + 1));
      if (tempPeriodTo.getDate() !== 1) {
        this.messageService.add({ severity: 'error', summary: 'Failed', detail: 'end date have to selected with end day of the month', life: 1 });
        return;
      }
    this.confirmationService.confirm({
      message: 'Are you sure you want to save this data ?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.authService.postMethod('/api/outletpanel/save', this.panelSpecialForm.value).subscribe({
          next: data => {
            this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Data has been saved', life: 500 });
            const today: Date = new Date();
            const tempStartDate: Date = new Date(Date.UTC(today.getFullYear(), today.getMonth(), 1));
            const tempEndDate: Date = new Date(Date.UTC((today.getFullYear() + 10), 11, 31));
            this.panelSpecialForm = this.formBuilder.group({
              area: ['', Validators.required],
              lini: ['', Validators.required],
              customerArea: ['', Validators.required],
              channel: ['', Validators.required],
              StartDate: [tempStartDate, Validators.required],
              EndDate: [tempEndDate, Validators.required],
              customerList: this.formBuilder.array([])
            });
            this.selectedCustomerList = [];
          },
          error: error => {
            if (error.error.code === 'INVALID TOKEN') {
              this.authService.clear();
            } else {
              this.messageService.add({key: 'globalToast', severity:'error', summary: 'Error', detail: error.error.message});
            }
          }
        });
      }
    });
  }
}