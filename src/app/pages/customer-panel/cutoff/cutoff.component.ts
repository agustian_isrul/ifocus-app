import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ConfirmationService, LazyLoadEvent, Message, MessageService } from 'primeng/api';
import { Table } from 'primeng/table';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-cutoff',
  templateUrl: './cutoff.component.html',
  styleUrls: ['./cutoff.component.css']
})
export class CutoffComponent implements OnInit {

  formMessage: Message[] = [];
  loading: boolean[] = [false, false, false, false];
  totalRecords: number;
  areaList: any[];
  liniList: any[];
  channelList: any[];
  cutOffPanelForm: FormGroup;
  panelList: any[];
  selectedPanelList: any[];

  constructor(private formBuilder: FormBuilder, public datePipe: DatePipe,
    private messageService: MessageService, private confirmationService: ConfirmationService,
    private authService: AuthService) {
    this.totalRecords = 0;
    this.areaList = [];
    this.liniList = [];
    this.channelList = [];
    this.panelList = [];
    this.selectedPanelList = [];
    this.cutOffPanelForm = this.formBuilder.group({
      area: ['', Validators.required],
      lini: ['', Validators.required],
      channel: ['', Validators.required],
      CustName: ['']
    });
  }

  ngOnInit(): void {
    this.onLoadArea();
    this.onLoadLine();
    this.onLoadChannel();
  }

  onLoadArea() {
    this.loading[0] = true;
    this.authService.postMethod('/api/dexaapi/area/list', {})
      .subscribe({
        next: data => {
          this.loading[0] = false;
          if (data) {
            this.areaList = data;
          } else {
            this.areaList = [{
              AreaID: '',
              AreaCode: "",
              AreaName: "No Data Found"
            }];
          }
        },
        error: error => {
          this.loading[0] = false;
          this.areaList = [{
            AreaID: '',
            AreaCode: "",
            AreaName: "No Data Found"
          }];
          if (error.error.code === 'INVALID TOKEN') {
            this.authService.clear();
          } else {
            this.messageService.add({ key: 'globalToast', severity: 'error', summary: 'Error', detail: error.error.message });
          }
        }
      });
  }

  onLoadLine() {
    this.loading[1] = true;
    this.authService.postMethod('/api/dexaapi/line/list', {})
      .subscribe({
        next: data => {
          this.loading[1] = false;
          if (data) {
            this.liniList = data;
          } else {
            this.liniList = [{
              LineID: '',
              LineName: "No Data Found"
            }];
          }
        },
        error: error => {
          this.loading[1] = false;
          this.liniList = [{
            LineID: '',
            LineName: "No Data Found"
          }];
          if (error.error.code === 'INVALID TOKEN') {
            this.authService.clear();
          } else {
            this.messageService.add({ key: 'globalToast', severity: 'error', summary: 'Error', detail: error.error.message });
          }
        }
      });
  }

  onLoadChannel() {
    this.loading[2] = true;
    this.authService.postMethod('/api/dexaapi/channel/list', {})
      .subscribe({
        next: data => {
          this.loading[2] = false;
          if (data) {
            this.channelList = data;
          } else {
            this.channelList = [{
              ChannelID: '',
              ChannelCode: "",
              ChannelName: "No Data Found",
              GroupName: ""
            }];
          }
        },
        error: error => {
          this.loading[2] = false;
          this.channelList = [{
            ChannelID: '',
            ChannelCode: "",
            ChannelName: "No Data Found",
            GroupName: ""
          }];
          if (error.error.code === 'INVALID TOKEN') {
            this.authService.clear();
          } else {
            this.messageService.add({ key: 'globalToast', severity: 'error', summary: 'Error', detail: error.error.message });
          }
        }
      });
  }

  onSearch(table: Table) {
    table.reset();
  }

  lazyLoadOutletPanel(event: LazyLoadEvent) {
    if (event && this.cutOffPanelForm.valid) {
      this.loading[3] = true;
      this.authService.postMethod('/api/outletpanel/search', {
        outletPanelForm: this.cutOffPanelForm.value,
        pageSetting: event
      }).subscribe({
        next: data => {
          if (data && data.length > 0) {
            this.loading[3] = false;
            this.panelList = data;
            this.totalRecords = data[0].totalRows;
          } else {
            this.loading[3] = false;
            this.totalRecords = 0;
            this.panelList = [];
          }
        }, error: error => {
          this.loading[3] = false;
          this.totalRecords = 0;
          this.panelList = [];
          if (error.error.code === 'INVALID TOKEN') {
            this.authService.clear();
          } else {
            this.messageService.add({ key: 'globalToast', severity: 'error', summary: 'Error', detail: error.error.message });
          }
        }
      });
    }
  }

  onCutOff() {
    this.confirmationService.confirm({
      message: 'Are you sure you want to cut off this data ?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.selectedPanelList.forEach(item => {
          const tempEndDate = new Date(item.EndDate);//.toLocaleDateString('in-ID', {timeZone: 'Asia/Jakarta'});
          
          console.log("<<<<<tempEndDate>>>>", tempEndDate);
          item.EndDate = new Date(Date.UTC(
            tempEndDate.getFullYear(),
            tempEndDate.getMonth(),
            tempEndDate.getDate()));
        });
        this.authService.postMethod('/api/outletpanel/cutoff', this.selectedPanelList).subscribe({
          next: data => {
            this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Data has been cut off', life: 3000 });
            this.panelList = [];
            this.selectedPanelList = [];
            this.cutOffPanelForm = this.formBuilder.group({
              area: ['', Validators.required],
              lini: ['', Validators.required],
              channel: ['', Validators.required],
              CustName: ['', Validators.required]
            });
          },
          error: error => {
            if (error.error.code === 'INVALID TOKEN') {
              this.authService.clear();
            } else {
              this.messageService.add({ key: 'globalToast', severity: 'error', summary: 'Error', detail: error.error.message });
            }
          }
        });
      }
    });
  }
}
