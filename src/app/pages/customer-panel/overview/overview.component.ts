import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LazyLoadEvent, Message, MessageService } from 'primeng/api';
import { Table } from 'primeng/table';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.css']
})
export class OverviewComponent implements OnInit {

  totalRecords: number;
  formMessage: Message[] = [];
  loading: boolean[] = [false, false, false, false];
  areaList: any[];
  liniList: any[];
  channelList: any[];
  statusList: any[];
  overviewPanelForm: FormGroup;
  panelList: any[];
  selectedPanelList: any[];

  constructor(private formBuilder: FormBuilder, public datePipe: DatePipe,
    private authService: AuthService, private messageService: MessageService) {
    this.totalRecords = 0;
    const today: Date = new Date();
    const startDate: Date = new Date(Date.UTC(today.getFullYear(), today.getMonth(), 1));
    const endDate: Date = new Date(Date.UTC(today.getFullYear(), today.getMonth() + 1, 0));
    this.areaList = [];
    this.liniList = [];
    this.channelList = [];
    this.statusList = [
      {name: 'Non Active'},
      {name: 'Active'}
    ];
    this.overviewPanelForm = this.formBuilder.group({
      area: [''],
      lini: [''],
      channel: [''],
      status: [''],
      periodFrom: [startDate],
      periodTo: [endDate]
    });
    this.panelList = [];
    this.selectedPanelList = [];
  }

  ngOnInit(): void {
    this.onLoadArea();
    this.onLoadLine();
    this.onLoadChannel();
  }

  onLoadArea() {
    this.loading[0] = true;
    this.authService.postMethod('/api/dexaapi/area/list', {})
      .subscribe({
        next: data => {
          this.loading[0] = false;
          if (data) {
            this.areaList = data;
          } else {
            this.areaList = [{
              AreaID: '',
              AreaCode: "",
              AreaName: "No Data Found"
            }];
          }
        },
        error: error => {
          this.loading[0] = false;
          this.areaList = [{
            AreaID: '',
            AreaCode: "",
            AreaName: "No Data Found"
          }];
          if (error.error.code === 'INVALID TOKEN') {
            this.authService.clear();
          } else {
            this.messageService.add({ key: 'globalToast', severity: 'error', summary: 'Error', detail: error.error.message });
          }
        }
      });
  }

  onLoadLine() {
    this.loading[1] = true;
    this.authService.postMethod('/api/dexaapi/line/list', {})
      .subscribe({
        next: data => {
          this.loading[1] = false;
          if (data) {
            this.liniList = data;
          } else {
            this.liniList = [{
              LineID: '',
              LineName: "No Data Found"
            }];
          }
        },
        error: error => {
          this.loading[1] = false;
          this.liniList = [{
            LineID: '',
            LineName: "No Data Found"
          }];
          if (error.error.code === 'INVALID TOKEN') {
            this.authService.clear();
          } else {
            this.messageService.add({ key: 'globalToast', severity: 'error', summary: 'Error', detail: error.error.message });
          }
        }
      });
  }

  onLoadChannel() {
    this.loading[2] = true;
    this.authService.postMethod('/api/dexaapi/channel/list', {})
      .subscribe({
        next: data => {
          this.loading[2] = false;
          if (data) {
            this.channelList = data;
          } else {
            this.channelList = [{
              ChannelID: '',
              ChannelCode: "",
              ChannelName: "No Data Found",
              GroupName: ""
            }];
          }
        },
        error: error => {
          this.loading[2] = false;
          this.channelList = [{
            ChannelID: '',
            ChannelCode: "",
            ChannelName: "No Data Found",
            GroupName: ""
          }];
          if (error.error.code === 'INVALID TOKEN') {
            this.authService.clear();
          } else {
            this.messageService.add({ key: 'globalToast', severity: 'error', summary: 'Error', detail: error.error.message });
          }
        }
      });
  }

  onSearch(table: Table) {
    table.reset();
  }

  lazyLoadOutletPanel(event: LazyLoadEvent) {
    if (event) {
      this.loading[3] = true;
      this.authService.postMethod('/api/outletpanel/search', {
        outletPanelForm: this.overviewPanelForm.value,
        pageSetting: event
      }).subscribe({
        next: data => {
          if (data && data.length > 0) {
            this.loading[3] = false;
            this.panelList = data;
            this.totalRecords = data[0].totalRows;
          } else {
            this.loading[3] = false;
            this.totalRecords = 0;
            this.panelList = [];
          }
        }, error: error => {
          this.loading[3] = false;
          this.totalRecords = 0;
          this.panelList = [];
          if (error.error.code === 'INVALID TOKEN') {
            this.authService.clear();
          } else {
            this.messageService.add({ key: 'globalToast', severity: 'error', summary: 'Error', detail: error.error.message });
          }
        }
      });
    }
  }
}
