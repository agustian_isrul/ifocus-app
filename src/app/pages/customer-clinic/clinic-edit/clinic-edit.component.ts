import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { AuthService } from 'src/app/auth/auth.service';
import { CustomerClinicModalComponent } from '../../popup/customerclinic/customerclinicmodal.component';

@Component({
  selector: 'app-clinic-edit',
  templateUrl: './clinic-edit.component.html',
  styleUrls: ['./clinic-edit.component.css']
})
export class ClinicEditComponent implements OnInit {

  areaList: any[];
  liniList: any[];
  statusClinic: any[];
  panelForm: FormGroup;
  customerDialog: DynamicDialogRef = new DynamicDialogRef;
  selectedCustomerList: any[] = [];
  selectedPanelList: any[] = [];
  filterPanelList:any[];
  panelList:any[];
  endingdate: string = '';

  constructor(private formBuilder: FormBuilder, public dialogService: DialogService, 
    public datePipe: DatePipe, private messageService: MessageService,
    private confirmationService: ConfirmationService, private authService: AuthService) {
    const today:Date = new Date();
    const startDate:Date = new Date(today.getFullYear(), today.getMonth(), 1);
    const endDate:Date = new Date((today.getFullYear() + 10), 11, 31);
    this.areaList = [];
    this.liniList = [];
    this.statusClinic = [
      {id: '1', name: 'Active'},
      {id: '2', name: 'Deactive'}
    ];

    this.filterPanelList = [
      {id: '95891', name: 'Outlet Name 1', address: 'Alamat Outlet 1', areaId: '1', areaName: 'ACEH', liniId: '1', liniName: 'VICTORY', channelId: '1', channelName: 'Panel APKS', startDate: new Date('2022-01-01T00:00:00'), endDate: new Date('2032-12-31T00:00:00'), statusId: '0', statusName: 'New Request',clinicName: 'Klinik Prima',clinicAddress: 'Jl Raya Bogor1'},
      {id: '23112', name: 'Outlet Name 5', address: 'Alamat Outlet 5', areaId: '1', areaName: 'ACEH', liniId: '1', liniName: 'VICTORY', channelId: '1', channelName: 'Panel APKS', startDate: new Date('2022-01-01T00:00:00'), endDate: new Date('2032-12-31T00:00:00'), statusId: '0', statusName: 'New Request',clinicName: 'Klinik Nurmala',clinicAddress: 'Jl Raya Bogor2'},
      {id: '23423', name: 'Outlet Name 5', address: 'Alamat Outlet 5', areaId: '1', areaName: 'ACEH', liniId: '1', liniName: 'VICTORY', channelId: '1', channelName: 'Panel APKS', startDate: new Date('2022-01-01T00:00:00'), endDate: new Date('2032-12-31T00:00:00'), statusId: '0', statusName: 'New Request',clinicName: 'Klinik Media CM',clinicAddress: 'Jl Raya Bogor3'},
      {id: '23442', name: 'Outlet Name 5', address: 'Alamat Outlet 5', areaId: '1', areaName: 'ACEH', liniId: '1', liniName: 'VICTORY', channelId: '1', channelName: 'Panel APKS', startDate: new Date('2022-01-01T00:00:00'), endDate: new Date('2032-12-31T00:00:00'), statusId: '0', statusName: 'New Request',clinicName: 'Klinik Umum Gihi',clinicAddress: 'Jl Raya Bogor4'},
      {id: '95892', name: 'Outlet Name 2', address: 'Alamat Outlet 2', areaId: '1', areaName: 'ACEH', liniId: '2', liniName: 'ZETA', channelId: '2', channelName: 'Panel Special', startDate: new Date('2022-01-01T00:00:00'), endDate: new Date('2032-12-31T00:00:00'), statusId: '1', statusName: 'Non Active',clinicName: 'Klinik Medical Center',clinicAddress: 'Jl Raya Bogor5'},
      {id: '95893', name: 'Outlet Name 3', address: 'Alamat Outlet 3', areaId: '2', areaName: 'JAKARTA', liniId: '1', liniName: 'VICTORY', channelId: '1', channelName: 'Panel APKS', startDate: new Date('2022-01-01T00:00:00'), endDate: new Date('2032-12-31T00:00:00'), statusId: '2', statusName: 'Active',clinicName: 'Klinik Adam Jaya',clinicAddress: 'Jl Raya Bogor6'},
      {id: '95894', name: 'Outlet Name 4', address: 'Alamat Outlet 4', areaId: '2', areaName: 'JAKARTA', liniId: '2', liniName: 'ZETA', channelId: '2', channelName: 'Panel Special', startDate: new Date('2022-01-01T00:00:00'), endDate: new Date('2032-12-31T00:00:00'), statusId: '3', statusName: 'Rejected',clinicName: 'Klinik Sehat Ibu',clinicAddress: 'Jl Raya Bogor7'},
      {id: '95895', name: 'Outlet Name 5', address: 'Alamat Outlet 5', areaId: '1', areaName: 'ACEH', liniId: '1', liniName: 'VICTORY', channelId: '1', channelName: 'Panel APKS', startDate: new Date('2022-01-01T00:00:00'), endDate: new Date('2032-12-31T00:00:00'), statusId: '0', statusName: 'New Request',clinicName: 'Klinik Sunat Masal',clinicAddress: 'Jl Raya Bogor8'},
      {id: '95896', name: 'Outlet Name 6', address: 'Alamat Outlet 6', areaId: '1', areaName: 'ACEH', liniId: '2', liniName: 'ZETA', channelId: '2', channelName: 'Panel Special', startDate: new Date('2022-01-01T00:00:00'), endDate: new Date('2032-12-31T00:00:00'), statusId: '1', statusName: 'Non Active',clinicName: 'Klinik Dinas Kesehatan',clinicAddress: 'Jl Raya Bogor9'},
      {id: '95897', name: 'Outlet Name 7', address: 'Alamat Outlet 7', areaId: '2', areaName: 'JAKARTA', liniId: '1', liniName: 'VICTORY', channelId: '1', channelName: 'Panel APKS', startDate: new Date('2022-01-01T00:00:00'), endDate: new Date('2032-12-31T00:00:00'), statusId: '2', statusName: 'Active',clinicName: 'Klinik Kodim',clinicAddress: 'Jl Raya Bogor'},
      {id: '95898', name: 'Outlet Name 8', address: 'Alamat Outlet 8', areaId: '2', areaName: 'JAKARTA', liniId: '2', liniName: 'ZETA', channelId: '2', channelName: 'Panel Special', startDate: new Date('2022-01-01T00:00:00'), endDate: new Date('2032-12-31T00:00:00'), statusId: '3', statusName: 'Rejected',clinicName: 'Klinik Ibu dan Anak',clinicAddress: 'Jl Raya Bogor1'}
    ];
    this.panelList = [];


    this.panelForm = this.formBuilder.group({
      area: ['', Validators.required],
      lini: ['', Validators.required],
      clinicName: ['', Validators.required],
      clinicAddress: ['', Validators.required],
      customerName: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    this.authService.getMethod('/api/panelnormal/init')
    .subscribe({
      next: data => {
        if (data) {
          this.areaList = data.areaList;
          this.liniList = data.liniList;
          
        } else {
          this.areaList = [];
          this.liniList = [];
          
        }
      },
      error: error => {
        this.areaList = [];
        this.liniList = [];
       
      }
    });
  }

  get customerList() {
    return this.panelForm.get('customerList') as FormArray;
  }

  onAddCustomer() {
    let tempArea = this.panelForm.value.area;
    let tempLini = this.panelForm.value.lini;
    if (tempArea !== '') {
      this.customerDialog = this.dialogService.open(CustomerClinicModalComponent, {
        header: 'Customer List',
        width: '70%',
        contentStyle: {"max-height": "500px", "overflow": "auto"},
        baseZIndex: 10000,
        data: {
          CompanyID: this.authService.getStorage("CompanyID"),
          CompanyName: this.authService.getStorage("CompanyName"),
          areaId: tempArea.areaId,
          areaName: tempArea.areaName,
          liniId:tempLini.liniId,
          liniName:tempLini.liniName
        }
      });
  
      this.customerDialog.onClose.subscribe((selectedDialog: any) => {
        if (selectedDialog !== undefined) {
          console.log(selectedDialog);
          //this.panelForm.value.customerName = selectedDialog.custName;
          this.endingdate = selectedDialog.custName;
          console.log(this.endingdate);
          this.panelForm.controls['customerName'].setValue(selectedDialog.custName);
        }
      });
    }
  }


  onSearch() {

    this.panelList = this.filterPanelList.filter((item: any) => {
      return item.areaId.includes(this.panelForm.value.area.areaId) &&
      item.liniId.includes(this.panelForm.value.lini.liniId) &&
      item.clinicName.includes(this.panelForm.value.clinicName) &&
      item.clinicAddress.includes(this.panelForm.value.clinicAddress) &&
      ((this.panelForm.value.customerName !== '') ? item.name.includes(this.panelForm.value.customerName) : 1 == 1);
    });

    /*
    let payload = {
      areaId: this.panelForm.value.area.areaId,
      liniId:this.panelForm.value.lini.liniId,
      ouletName:this.panelForm.value.channel.customerName
    };

    console.log(this.filterPanelList);
    
    this.authService.postMethod('/api/panelcutoff/searchPanel',payload)
    .subscribe({
      next: data => {
      this.filterPanelList = data.data.panelList; 
      console.log(this.filterPanelList);

      this.panelList = this.filterPanelList.filter((item: any) => {
        return item.areaId.includes(this.cutOffPanelForm.value.area.areaId) && 
        item.channelId.includes(this.cutOffPanelForm.value.channel.channelId) &&
        ((this.cutOffPanelForm.value.customerName !== '') ? item.outletName.toLowerCase().includes(this.cutOffPanelForm.value.customerName) : 1 == 1);
      });
    }});
    */
    
  }

  ngOnDestroy() {
    if (this.customerDialog) {
        this.customerDialog.close();
    }
  }

  onDeleteCustomer() {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete this data ?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        let result = this.customerList.value.filter((item: any) => 
        !this.selectedCustomerList.includes(item));
        this.customerList.setValue([]);        
        result.filter((item : any) => this.customerList.value.push(item));
        this.messageService.add({severity:'success', summary: 'Successful', detail: 'Data has been deleted', life: 3000});
        this.selectedCustomerList = [];
      }
    });
  }

  onSubmit() {
    this.confirmationService.confirm({
      message: 'Are you sure you want to save this data ?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.messageService.add({severity:'success', summary: 'Successful', detail: 'Data has been saved', life: 3000});
        const today:Date = new Date();

        this.panelForm = this.formBuilder.group({
          area: ['', Validators.required],
          lini: ['', Validators.required],       
          customerName: ['', Validators.required],
          clinicName: ['', Validators.required],
          clinicAddress: ['', Validators.required],
          clinicStatus: ['', Validators.required]
        });
        this.panelList = [];
      }
    });
    
  }

}
