import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-add-new-clinic',
  templateUrl: './add-new-clinic.component.html',
  styleUrls: ['./add-new-clinic.component.css']
})
export class AddNewClinicComponent implements OnInit {

  areaList: any[];
  liniList: any[];
  clinicPanelForm: FormGroup;
  customerDialog: DynamicDialogRef = new DynamicDialogRef;
  selectedCustomerList: any[] = [];
  selectedClinicList: any[] = [];
  areaSelected: any = {};


  constructor(private formBuilder: FormBuilder, public dialogService: DialogService, 
    public datePipe: DatePipe, private messageService: MessageService,
    private confirmationService: ConfirmationService) {

      this.areaList = [
        {id: '1', name: 'ACEH'},
        {id: '2', name: 'JAKARTA'},
        {id: '3', name: 'BOGOR'}
      ];
      this.liniList = [
        {id: '1', name: 'VICTORY'},
        {id: '2', name: 'ZETA'}
      ];
      this.clinicPanelForm = this.formBuilder.group({
        incentive_periode: ['', Validators.required],
        area: ['', Validators.required],
        lini: ['', Validators.required],
        clinic_name: ['', Validators.required],
        clinic_address: ['', Validators.required],
        periodFrom: [new Date('2022-01-01T00:00:00'), Validators.required],
        periodTo: [new Date('2032-12-31T00:00:00'), Validators.required],
        customerList: this.formBuilder.array([])
      });
      
     } 

  ngOnInit(): void {
  }

  get customerList() {
    return this.clinicPanelForm.get('customerList') as FormArray;
  }

  

  ngOnDestroy() {
    if (this.customerDialog) {
        this.customerDialog.close();
    }
  }

  onSubmit() {
    this.confirmationService.confirm({
      message: 'Are you sure you want to save this data ?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        console.log(this.clinicPanelForm.value);
        this.selectedClinicList.push(this.clinicPanelForm.value);
        this.messageService.add({severity:'success', summary: 'Successful', detail: 'Data has been saved', life: 3000});
        this.clinicPanelForm = this.formBuilder.group({
          incentive_periode: ['', Validators.required],
          area: ['', Validators.required],
          lini: ['', Validators.required],
          clinic_name: ['', Validators.required],
          clinic_address: ['', Validators.required],          
          periodFrom: [new Date('2022-01-01T00:00:00'), Validators.required],
          periodTo: [new Date('2032-12-31T00:00:00'), Validators.required],
          customerList: this.formBuilder.array([]) 
        });
        
      }
    });
  }

  onDeleteClinic() {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete this data ?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        let result = this.customerList.value.filter((item: any) => !this.selectedClinicList.includes(item));
        this.customerList.setValue([]);        
        result.filter((item : any) => this.customerList.value.push(item));
        this.messageService.add({severity:'success', summary: 'Successful', detail: 'Data has been deleted', life: 3000});
        this.selectedClinicList = [];
      }
    });
  }

}

