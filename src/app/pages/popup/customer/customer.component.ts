import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { LazyLoadEvent, MessageService } from 'primeng/api';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { Table } from 'primeng/table';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent {

  totalRecords: number;
  loading: boolean = false;
  customerList: any[];
  selectedCustomerList: any[];
  filterCustomerName = new FormControl('20  KEMBANG GRE');
  private formBody : any;

  constructor(public ref: DynamicDialogRef, public config: DynamicDialogConfig,
    private authService: AuthService, private messageService: MessageService) {
    this.totalRecords = 0;
    this.customerList = [];
    this.selectedCustomerList = [];
    this.formBody = {
      CompanyID: this.config.data.CompanyID,
      AreaID: this.config.data.areaId,
      CustName: "",
      Type: "",
      StartRowIndex: 1
    }
  }

  searchCustomerName(table: Table) {
    table.reset();
  }

  lazyLoadCustomer(event: LazyLoadEvent) {
    if (event) {
      this.loading = true;
      if (this.filterCustomerName.value === '') {
        this.formBody.CustName = "";
      } else {
        this.formBody.CustName = JSON.stringify(this.filterCustomerName.value.toLowerCase());
      }
      this.formBody.StartRowIndex = ((event.first || 0) + (event.rows || 10)) / (event.rows || 10);
      this.authService.postMethod('/api/dexaapi/customeroutlet/list', this.formBody)
      .subscribe({
        next: data => {
          this.loading = false;
          if (data) {
            this.customerList = data;
            this.totalRecords = data[0].TotalRows;
          } else {
            this.totalRecords = 0;
            this.customerList = [];
          }
        },
        error: error => {
          this.loading = false;
          this.totalRecords = 0;
          this.customerList = [];
          if (error.error.code === 'INVALID TOKEN') {
            this.authService.clear();
          } else {
            this.messageService.add({key: 'globalToast', severity:'error', summary: 'Error', detail: error.error.message});
          }
        }
      });
    }
  }

  addCustomer() {
    this.ref.close(this.selectedCustomerList);
  }
}
