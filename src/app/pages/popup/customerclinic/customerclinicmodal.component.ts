import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { LazyLoadEvent, PrimeNGConfig } from 'primeng/api';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-customer',
  templateUrl: './customerclinicmodal.component.html',
  styleUrls: ['./customerclinicmodal.component.css']
})
export class CustomerClinicModalComponent {

  totalRecords: number;
  loading: boolean;
  customerList: any[];
  customerListData: any[];
  selectedCustomerList: any[];
  filterCustomerName = new FormControl('');
  private formBody : any;

  constructor(public ref: DynamicDialogRef, public config: DynamicDialogConfig,
    private authService: AuthService, private primengConfig: PrimeNGConfig) {
    this.totalRecords = 0;
    this.loading = true;
    this.customerList = [];

    this.customerListData = [
      {outletId: '95891', custName: 'Outlet Name 1', address: 'Alamat Outlet 1', areaoutletId: '1', areaName: 'ACEH', linioutletId: '1', liniName: 'VICTORY', channeloutletId: '1', channelName: 'Panel APKS', startDate: new Date('2022-01-01T00:00:00'), endDate: new Date('2032-12-31T00:00:00'), statusoutletId: '0', statusName: 'New Request',clinicName: 'Klinik Prima',clinicAddress: 'Jl Raya Bogor'},
      {outletId: '95892', custName: 'Outlet Name 2', address: 'Alamat Outlet 2', areaoutletId: '1', areaName: 'ACEH', linioutletId: '2', liniName: 'ZETA', channeloutletId: '2', channelName: 'Panel Special', startDate: new Date('2022-01-01T00:00:00'), endDate: new Date('2032-12-31T00:00:00'), statusoutletId: '1', statusName: 'Non Active',clinicName: 'Klinik Medical Center',clinicAddress: 'Jl Raya Bogor'},
      {outletId: '95893', custName: 'Outlet Name 3', address: 'Alamat Outlet 3', areaoutletId: '2', areaName: 'JAKARTA', linioutletId: '1', liniName: 'VICTORY', channeloutletId: '1', channelName: 'Panel APKS', startDate: new Date('2022-01-01T00:00:00'), endDate: new Date('2032-12-31T00:00:00'), statusoutletId: '2', statusName: 'Active',clinicName: 'Klinik Adam Jaya',clinicAddress: 'Jl Raya Bogor'},
      {outletId: '95894', custName: 'Outlet Name 4', address: 'Alamat Outlet 4', areaoutletId: '2', areaName: 'JAKARTA', linioutletId: '2', liniName: 'ZETA', channeloutletId: '2', channelName: 'Panel Special', startDate: new Date('2022-01-01T00:00:00'), endDate: new Date('2032-12-31T00:00:00'), statusoutletId: '3', statusName: 'Rejected',clinicName: 'Klinik Sehat Ibu',clinicAddress: 'Jl Raya Bogor'},
      {outletId: '95895', custName: 'Outlet Name 5', address: 'Alamat Outlet 5', areaoutletId: '1', areaName: 'ACEH', linioutletId: '1', liniName: 'VICTORY', channeloutletId: '1', channelName: 'Panel APKS', startDate: new Date('2022-01-01T00:00:00'), endDate: new Date('2032-12-31T00:00:00'), statusoutletId: '0', statusName: 'New Request',clinicName: 'Klinik Sunat Masal',clinicAddress: 'Jl Raya Bogor'},
      {outletId: '95896', custName: 'Outlet Name 6', address: 'Alamat Outlet 6', areaoutletId: '1', areaName: 'ACEH', linioutletId: '2', liniName: 'ZETA', channeloutletId: '2', channelName: 'Panel Special', startDate: new Date('2022-01-01T00:00:00'), endDate: new Date('2032-12-31T00:00:00'), statusoutletId: '1', statusName: 'Non Active',clinicName: 'Klinik Dinas Kesehatan',clinicAddress: 'Jl Raya Bogor'},
      {outletId: '95897', custName: 'Outlet Name 7', address: 'Alamat Outlet 7', areaoutletId: '2', areaName: 'JAKARTA', linioutletId: '1', liniName: 'VICTORY', channeloutletId: '1', channelName: 'Panel APKS', startDate: new Date('2022-01-01T00:00:00'), endDate: new Date('2032-12-31T00:00:00'), statusoutletId: '2', statusName: 'Active',clinicName: 'Klinik Kodim',clinicAddress: 'Jl Raya Bogor'},
      {outletId: '95898', custName: 'Outlet Name 8', address: 'Alamat Outlet 8', areaoutletId: '2', areaName: 'JAKARTA', linioutletId: '2', liniName: 'ZETA', channeloutletId: '2', channelName: 'Panel Special', startDate: new Date('2022-01-01T00:00:00'), endDate: new Date('2032-12-31T00:00:00'), statusoutletId: '3', statusName: 'Rejected',clinicName: 'Klinik Ibu dan Anak',clinicAddress: 'Jl Raya Bogor'}
    ];
    this.selectedCustomerList = [];
    this.formBody = {
      CompanyID: this.config.data.CompanyID,
      AreaID: this.config.data.areaId,
      CustName: "",
      Type: "",
      StartRowIndex: 1
    }
  }

  searchCustomerName() {
    this.customerList = this.customerListData;
    /*
    this.loading = true;
    if (this.filterCustomerName.value === '') {
      this.formBody.CustName = "";
    } else {
      this.formBody.CustName = this.filterCustomerName.value.toLowerCase();
    }
    this.authService.postMethod('/api/customeroutlet/list', this.formBody)
    .subscribe({
      next: data => {
        if (data && data.statusCode === 200) {
          this.loading = false;
          this.customerList = data.body;
          this.totalRecords = data.body[0].TotalRows;
        } else {
          this.loading = false;
          this.totalRecords = 0;
          this.customerList = [];
        }
      },
      error: error => {
        this.loading = false;
        this.totalRecords = 0;
        this.customerList = [];
      }
    });*/
  }
  
  lazyLoadCustomer(event: LazyLoadEvent) {
    /*
    this.loading = true;
    if (this.filterCustomerName.value === '') {
      this.formBody.CustName = "";
    } else {
      this.formBody.CustName = this.filterCustomerName.value.toLowerCase();
    }
    this.authService.postMethod('/api/customeroutlet/list', this.formBody)
    .subscribe({
      next: data => {
        if (data && data.statusCode === 200) {
          this.loading = false;
          this.customerList = data.body;
          this.totalRecords = data.body[0].TotalRows;
        } else {
          this.loading = false;
          this.totalRecords = 0;
          this.customerList = [];
        }
      },
      error: error => {
        this.loading = false;
        this.totalRecords = 0;
        this.customerList = [];
      }
    });
    */
  }

  onSelectionChangeOutlet(data:any) {
    console.log(data);
    this.ref.close(data[0]);
  }

  selectCustomer(data: any) {
    this.ref.close(this.selectedCustomerList);
  }
}
