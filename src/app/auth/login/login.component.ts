import { Component, OnInit, SkipSelf } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { AuthService, BROWSER_STORAGE } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [
    AuthService,
    { provide: BROWSER_STORAGE, useFactory: () => sessionStorage }
  ]
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  today: Date = new Date();
  loading: boolean = false;

  constructor(
    @SkipSelf() private authService: AuthService, private messageService: MessageService,
    private formBuilder: FormBuilder, public router: Router) {
    this.loginForm = this.formBuilder.group({
      Username: ['', Validators.required],
      Password: ['', Validators.required]
    });
    setInterval(() => {
      this.today = new Date();
    }, 1);
  }

  ngOnInit(): void {
  }

  onSubmit() {
    this.loading = true;
    this.authService.postMethod('/api/authenticate/login', this.loginForm.value)
    .subscribe({
      next: data => {
        this.loading = false;
        if (data && data.message) {
          this.messageService.add({severity:'error', summary: 'Error', detail: data.message, life: 3000});
        } else {
          Object.keys(data.profileUser).forEach((key) => {
            this.authService.setStorage(key, data.profileUser[key]);
          });        
          this.authService.setStorage('tokenUser', data.tokenUser);
          this.router.navigate(['/dashboard']);
        }
      },
      error: error => {
        this.loading = false;
        this.messageService.add({severity:'error', summary: 'Error', detail: error.message, life: 3000});
      }
    });
  }
}
