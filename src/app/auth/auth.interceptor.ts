import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, skip } from 'rxjs/operators';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private authService: AuthService) { }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    let authReq = request.clone({ setHeaders: { 'Content-Type': 'application/json' } });
    const tokenUser = this.authService.getStorage('tokenUser');
    if (tokenUser) {
      authReq = request.clone({
        setHeaders: {
          'Content-Type': 'application/json',
          'Authorization': tokenUser
        }
      });
    }
    return next.handle(authReq);
  }
}
