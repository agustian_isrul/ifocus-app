import { HttpClient } from '@angular/common/http';
import { Inject, Injectable, InjectionToken } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

export const BROWSER_STORAGE = new InjectionToken<Storage>('Browser Storage', {
  providedIn: 'root',
  factory: () => localStorage
});

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(@Inject(BROWSER_STORAGE) private storage: Storage, private httpClient: HttpClient,
  private router: Router) { }

  getStorage(key: string) {
    return this.storage.getItem(key);
    // const jsonString = this.storage.getItem(key);
    // if (jsonString) {
    //   return JSON.parse(jsonString);
    // }
    // return '';
  }

  setStorage(key: string, value: string) {
    this.storage.setItem(key, value);
  }

  removeStorage(key: string) {
    this.storage.removeItem(key);
  }

  clear() {
    this.storage.clear();
    this.router.navigate(['/login']);
  }

  postMethod(url: any, dataBody: any): Observable<any> {
	  return this.httpClient.post(url, dataBody);
    //return this.httpClient.post(url, dataBody).pipe(retry(1), catchError(this.handleError));
  }

  getMethod(url: any): Observable<any> {
	  return this.httpClient.get(url);
    //return this.httpClient.get(url).pipe(retry(1), catchError(this.handleError));
  }

  handleError(error: any) {
    let errorMessage = "";
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = "Error Code: ${error.status} \n Message: ${error.message}";
    }
    window.alert(errorMessage);
    return throwError(() => new Error(errorMessage));
  }
}
