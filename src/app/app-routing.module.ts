import { NgModule } from '@angular/core';
import { ExtraOptions, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth/auth.guard';
import { PreloadStrategyService } from './auth/preload-strategy.service';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./pages/pages.module').then(m => m.PagesModule),
    canLoad: [AuthGuard]
  },
  { path: '**', redirectTo: '' }
];

const config: ExtraOptions = {
  useHash: false,
  enableTracing: false,
  preloadingStrategy: PreloadStrategyService
};

@NgModule({
  imports: [RouterModule.forRoot(routes, config)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
